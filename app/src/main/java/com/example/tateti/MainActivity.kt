package com.example.tateti

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    var tateti = IntArray(9)
    var ban=0
    var ban2=0
    var jugador=1
    var resultado=""
    var texto1=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val mensaje: TextView= findViewById(R.id.textView3)
        val boton1: Button = findViewById(R.id.button)
        val boton2: Button = findViewById(R.id.button2)
        val boton3: Button = findViewById(R.id.button3)
        val boton4: Button = findViewById(R.id.button4)
        val boton5: Button = findViewById(R.id.button5)
        val boton6: Button = findViewById(R.id.button6)
        val boton7: Button = findViewById(R.id.button7)
        val boton8: Button = findViewById(R.id.button8)
        val boton9: Button = findViewById(R.id.button9)
        val boton10: Button = findViewById(R.id.button10)
        fun calculoganador(): String{
            if(tateti[0]==1 && tateti[1]==1 && tateti[2]==1){
                texto1="El ganador es el jugador nro 1 "
                mensaje.text= texto1
                ban=1
            }
            if(tateti[3]==1 && tateti[4]==1 && tateti[5]==1){
                texto1="El ganador es el jugador nro 1 "
                mensaje.text= texto1
                ban=1
            }
            if(tateti[6]==1 && tateti[7]==1 && tateti[8]==1){
                texto1="El ganador es el jugador nro 1 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[0]==1 && tateti[3]==1 && tateti[6]==1){
                texto1="El ganador es el jugador nro 1 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[1]==1 && tateti[4]==1 && tateti[7]==1){
                texto1="El ganador es el jugador nro 1 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[2]==1 && tateti[5]==1 && tateti[8]==1){
                texto1="El ganador es el jugador nro 1 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[0]==1 && tateti[4]==1 && tateti[8]==1){
                texto1="El ganador es el jugador nro 1 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[2]==1 && tateti[4]==1 && tateti[6]==1){
                texto1="El ganador es el jugador nro 1 "
                ban=1
                mensaje.text= texto1
            }
            //ahora para el jugador 2
            if(tateti[0]==2 && tateti[1]==2 && tateti[2]==2){
                texto1="El ganador es el jugador nro 2 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[3]==2 && tateti[4]==2 && tateti[5]==2){
                texto1="El ganador es el jugador nro 2 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[6]==2 && tateti[7]==2 && tateti[8]==2){
                texto1="El ganador es el jugador nro 2 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[0]==2 && tateti[3]==2 && tateti[6]==2){
                texto1="El ganador es el jugador nro 2 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[1]==2 && tateti[4]==2 && tateti[7]==2){
                texto1="El ganador es el jugador nro 2 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[2]==2 && tateti[5]==2 && tateti[8]==2){
                texto1="El ganador es el jugador nro 2 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[0]==2 && tateti[4]==2 && tateti[8]==2){
                texto1="El ganador es el jugador nro 2 "
                ban=1
                mensaje.text= texto1
            }
            if(tateti[2]==2 && tateti[4]==2 && tateti[6]==2){
                texto1="El ganador es el jugador nro 2 "
                ban=1
                mensaje.text= texto1
            }
            return mensaje.toString()
        }
        //botones
        boton1.setOnClickListener{
            if(jugador%2==0){
                boton1.text="X"
                tateti[0]=1

            }
            if(jugador%2!=0){
                boton1.text="O"
                tateti[0]=2
            }
            calculoganador()
            jugador++
            ban2 += 1
        }
        boton2.setOnClickListener{
            if(jugador%2==0){
                boton2.text="X"
                tateti[1]=1

            }
            if(jugador%2!=0){
                boton2.text="O"
                tateti[1]=2

            }
            calculoganador()
            jugador++
            ban2 += 1
        }
        boton3.setOnClickListener{
            if(jugador%2==0){
                boton3.text="X"
                tateti[2]=1
            }
            if(jugador%2!=0){
                boton3.text="O"
                tateti[2]=2
            }
            calculoganador()
            jugador++
            ban2 += 1
        }
        boton4.setOnClickListener{
            if(jugador%2==0){
                boton4.text="X"
                tateti[3]=1

            }
            if(jugador%2!=0){
                boton4.text="O"
                tateti[3]=2

            }
            calculoganador()
            jugador++
            ban2 += 1
        }
        boton5.setOnClickListener{
            if(jugador%2==0){
                boton5.text="X"
                tateti[4]=1

            }
            if(jugador%2!=0){
                boton5.text="O"
                tateti[4]=2

            }
            calculoganador()
            jugador++
            ban2 += 1
        }
        boton6.setOnClickListener{
            if(jugador%2==0){
                boton6.text="X"
                tateti[5]=1

            }
            if(jugador%2!=0){
                boton6.text="O"
                tateti[5]=2

            }
            calculoganador()
            jugador++
            ban2 += 1
        }
        boton7.setOnClickListener{
            if(jugador%2==0){
                boton7.text="X"
                tateti[6]=1

            }
            if(jugador%2!=0){
                boton7.text="O"
                tateti[6]=2

            }
            calculoganador()
            jugador++
            ban2 += 1
        }
        boton8.setOnClickListener{
            if(jugador%2==0){
                boton8.text="X"
                tateti[7]=1

            }
            if(jugador%2!=0){
                boton8.text="O"
                tateti[7]=2

            }
            calculoganador()
            jugador++
            ban2 += 1
        }
        boton9.setOnClickListener{
            if(jugador%2==0){
                boton9.text="X"
                tateti[8]=1

            }
            if(jugador%2!=0){
                boton9.text="O"
                tateti[8]=2

            }
            calculoganador()
            jugador++
            ban2 += 1
        }
        if(ban==9){
            resultado="Empatee :("
            mensaje.text=resultado
        }
        boton10.setOnClickListener{
            boton1.text=""
            boton2.text=""
            boton3.text=""
            boton4.text=""
            boton5.text=""
            boton6.text=""
            boton7.text=""
            boton8.text=""
            boton9.text=""
            for(i in tateti.indices){
                tateti[i]=0
            }
            jugador=1
            ban2=0
            mensaje.text=""
        }
    }


}